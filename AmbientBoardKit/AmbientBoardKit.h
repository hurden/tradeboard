//
//  AmbientBoardKit.h
//  AmbientBoardKit
//
//  Created by Taras Vozniuk on 9/4/14.
//  Copyright (c) 2014 ambientlight. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for AmbientBoardKit.
FOUNDATION_EXPORT double AmbientBoardKitVersionNumber;

//! Project version string for AmbientBoardKit.
FOUNDATION_EXPORT const unsigned char AmbientBoardKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AmbientBoardKit/PublicHeader.h>

