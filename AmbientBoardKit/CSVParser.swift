//
//  CSVParser.swift
//  Tradeboard
//
//
//  A simple CSVParser
//
//  Created by Taras Vozniuk on 8/28/14.
//  Copyright (c) 2014 ambientlight. All rights reserved.
//

import Foundation

public struct CSVParserOptions : RawOptionSetType, BooleanType {
    typealias RawValue = UInt
    private var value: UInt = 0
    init(_ value: UInt) { self.value = value }
    public init(rawValue value: UInt)  { self.value = value }
    public init(nilLiteral: ()) { self.value = 0 }
    public var rawValue: UInt { return self.value }
    
    public var boolValue: Bool  { return self.value != 0 }
    public func toRaw() -> UInt { return self.value }
    
    public static var allZeros: CSVParserOptions                   { return self(0) }
    public static func fromRaw(raw: UInt) -> CSVParserOptions?     { return self(raw) }
    public static func fromMask(raw: UInt) -> CSVParserOptions     { return self(raw) }
    public static func convertFromNilLiteral() -> CSVParserOptions { return self(0) }
    
    public static var None: CSVParserOptions                                       { return self(0) }
    public static var HeaderPresent: CSVParserOptions                              { return self(1 << 0) }
    public static var DoesNotRemoveDoublequoteFromStringLiterals: CSVParserOptions { return self(1 << 1) }
}

public class CSVParser
{
    public let headers: [String]?
    private let content: [[String]] = []
    
    let options: CSVParserOptions = .None
    let seperator = NSCharacterSet(charactersInString: ",")
    
    //indicates error has been encountered during parsing(couldn't fetch data from URL or emptyContent)
    public let didParseWithError = false
    
    public init(contentsOfURL url: NSURL, options: CSVParserOptions = .None, seperator: NSCharacterSet = NSCharacterSet(charactersInString: ","))
    {
        var error: NSError?
        if let csvString:String = NSString(contentsOfURL:url, encoding: NSUTF8StringEncoding, error: &error){
            
            self.seperator = seperator
            self.options = options
            
            
            let newlineCharacterSet: NSCharacterSet = NSCharacterSet.newlineCharacterSet()
            let rowLines: [String] = self.csvStringClearedFromExtraneousCharacters(originalString: csvString).componentsSeparatedByCharactersInSet(newlineCharacterSet)
            
            if rowLines.count == 0 {
                println("CSVParser: Warning: Could not parse rowLines(urlContentEmpty)")
                didParseWithError = true
            } else {
                headers = self.parseHeaders(fromLines: rowLines)
                content = self.parseContent(fromLines: rowLines)
            }
            
        } else {
            println("CSVParser: Error: Failed to fetch data from the URL(\(url.absoluteString)), \(error)")
            didParseWithError = true
        }
    }
    
    private func parseHeaders(fromLines rowLines: [String]) -> [String]?
    {
        if self.options & .HeaderPresent {
            return rowLines.first!.componentsSeparatedByCharactersInSet(self.seperator)
        }
        
        return nil
    }
    
    private func parseContent(fromLines rowLines: [String]) -> [[String]]
    {
        var parsedContent: [[String]] = []
        for (lineNumber:Int, line:String) in enumerate(rowLines) {
            
            if (self.options & .HeaderPresent) && lineNumber == 0 {
                continue
            }
            
            let rowCommaSeperatedComponents: [String] = line.componentsSeparatedByCharactersInSet(self.seperator)
            if rowCommaSeperatedComponents.count > 0 {
                parsedContent.append(rowCommaSeperatedComponents)
            }
        }
        
        return parsedContent
    }
    
    private func csvStringClearedFromExtraneousCharacters(var originalString originalStringAdjusted: String) -> String
    {
        //removing double quote from string literals
        if !(self.options & .DoesNotRemoveDoublequoteFromStringLiterals){
            originalStringAdjusted = originalStringAdjusted.stringByReplacingOccurrencesOfString("\"", withString: "")
        }
        
        //changing the DOS line seperator to normal
        originalStringAdjusted = originalStringAdjusted.stringByReplacingOccurrencesOfString("\r\n", withString: "\n")
        
        //trimming last newline characters
        let newlineCharacterSet: NSCharacterSet = NSCharacterSet.newlineCharacterSet()
        originalStringAdjusted = originalStringAdjusted.stringByTrimmingCharactersInSet(newlineCharacterSet)
        
        return originalStringAdjusted
    }
    
    
    public subscript(row: Int) -> [String]? {
        if row < self.content.count {
            return self.content[row]
        } else {
            return nil
        }
    }
    
    public subscript(row: Int, column: Int) -> String? {
        if row < self.content.count {
            if column < self.content[row].count {
                return self.content[row][column]
            }
        }
            
        return nil
    }
    
    public func row(rowIndex: Int) -> [String]? {
        return self[rowIndex]
    }
    public var rows: [[String]] {
        return self.content
    }
    
    public func rowPropertyList(forRowAtIndex rowIndex: Int) -> Dictionary<String, String>?
    {
        if (self.options & .HeaderPresent) && self.headers != nil{
            
            if rowIndex < self.content.count {
                
                if self.content[rowIndex].count == self.headers!.count {
                    return self.content[rowIndex].representativeDictionary(forKeys: self.headers!)
                } else {
                    // if somehow happen the we have different num headers then columns
                    // something terribly wrong
                    return nil
                }
            }
        }
        
        return nil
    }
    
    public func rowPropertyList(forRow row: [String]) -> Dictionary<String, String>?
    {
        if (self.options & .HeaderPresent) && self.headers != nil {
            
            if row.count == self.headers!.count {
                return row.representativeDictionary(forKeys: self.headers!)
            } else {
                // if somehow happen the we have different num headers then columns
                // something terribly wrong
                return nil
            }
        }
        
        return nil
    }
    
    public var rowPropertyLists: [Dictionary<String, String>?]?
    {
        if (self.options & .HeaderPresent) && self.headers != nil {
            
            return self.content.map { row in
                return self.rowPropertyList(forRow: row)
            }
        }
            
        return nil
    }
    
    public func column(forHeaderWithName headerName: String) -> [String?]?
    {
        if (self.options & .HeaderPresent) && self.headers != nil {
            
            if let headerIndex:Int = find(self.headers!, headerName){
                
                return self.content.map { row in
                    // still we have to check(what if that row is shorter: i.e value missing?)
                    // now if value is missing we must not skip that value
                    if headerIndex < row.count {
                        return row[headerIndex]
                    } else {
                        return nil
                    }
                }
            }
        }
        
        return nil
    }
    
    public func column(columnIndex: Int) -> [String?]?
    {
        if columnIndex < self.content.count {
            
            return self.content.map { row in
                if columnIndex < row.count {
                    return row[columnIndex]
                } else {
                    return nil
                }
            }
            
        } else {
            return nil
        }
    }
}









