//
//  extensions.swift
//  Tradeboard
//
//  Created by Taras Vozniuk on 8/29/14.
//  Copyright (c) 2014 ambientlight. All rights reserved.
//

import Foundation

public let oneDayTimeInterval:NSTimeInterval = 86400.0

extension String {
    var doubleValue: Double {
        return (self as NSString).doubleValue
    }
}

extension Array {

    func representativeDictionary<K: Hashable>(forKeys keys: [K]) -> Dictionary<K, T>?{
        if (keys.count < self.count){
            return nil
        } else {

            var resultingDict: Dictionary<K,T> = [:]
            for (index, element) in enumerate(self){
                resultingDict[keys[index]] = element
            }

            return resultingDict
        }
    }

    func mappedDictionary<K: Hashable, U>(forKeys keys: [K], transform: (T) -> U) -> Dictionary<K, U>? {
        if (keys.count < self.count){
            return nil
        } else {
            let mapping = self.map(transform)
            return mapping.representativeDictionary(forKeys: keys)
        }
    }
}

extension Dictionary /* <Key, Value> */ {
 
    func mapKeys<U> (transform: (Key) -> U) -> [U] {
        var results: [U] = []
        for k in self.keys {
            results.append(transform(k))
        }
        return results
    }
 
    func mapValues<U> (transform: (Value) -> U) -> [U] {
        var results: [U] = []
        for v in self.values {
            results.append(transform(v))
        }
        return results
    }
 
    func map<U> (transform: (Value) -> U) -> [U] {
        return self.mapValues(transform)
    }
 
    func map<U> (transform: (Key, Value) -> U) -> [U] {
        var results: [U] = []
        for k in self.keys {
            results.append(transform(k as Key, self[ k ]! as Value))
        }
        return results
    }
 
    func map<K: Hashable, V> (transform: (Key, Value) -> (K, V)) -> Dictionary<K, V> {
        var results: Dictionary<K, V> = [:]
        for k in self.keys {
            if let value = self[ k ] {
                let (u, w) = transform(k, value)
                results.updateValue(w, forKey: u)
            }
        }
        return results
    }
}


public func dispatchOnMainQueue(delay:Double, closure:()->()) {
    dispatch_after( dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), closure)
}