//
//  LiveQuotesCentre.swift
//  Tradeboard
//
//  Created by Taras Vozniuk on 8/27/14.
//  Copyright (c) 2014 ambientlight. All rights reserved.
//

import Foundation

//string literals for yahoo historic quotes
let csvHeaderDate = "Date"
let csvHeaderOpen = "Open"
let csvHeaderClose = "Close"
let csvHeaderHigh = "High"
let csvHeaderLow = "Low"
let csvHeaderVolume = "Volume"
let csvHeaderAdjustedClose = "Adj Close"

// MARK: - HistoricTimedQuote

public struct HistoricTimedQuote: Equatable
{
    public var symbol: String = String()
    public var symbolName: String = String()
    public var time: NSDate?
    public var timeInterval: NSTimeInterval = 0.0
    
    public var high: Double = 0.0
    public var low: Double = 0.0
    public var open: Double = 0.0
    public var close: Double = 0.0
    public var adjustedClose: Double = 0.0
    public var volume: Int = 0
    
    public var isValidQuote:Bool {
        return (countElements(symbol) > 0 && time != nil && high > 0 && low > 0 && open > 0 && close > 0)
    }
    
    public init(){}
    
    public init(symbol: String, symbolName: String, timeInterval: NSTimeInterval, propertyList list: Dictionary<String, String>)
    {
        self.symbol = symbol
        self.symbolName = symbolName
        self.timeInterval = timeInterval
        
        //we use pattern matching in typles(date, high, low, open, close), because if we do not have one of these properties, the quote is useless
        switch (list[csvHeaderDate], list[csvHeaderOpen], list[csvHeaderClose], list[csvHeaderHigh], list[csvHeaderLow]) {
            case (.Some(let dateString), .Some(let openString), .Some(let closeString), .Some(let highString), .Some(let lowString)):
            
                var dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "YYYY-MM-dd"
                dateFormatter.timeZone = NSTimeZone.systemTimeZone()
            
                if let parsedTime = dateFormatter.dateFromString(dateString){
                    self.time = parsedTime
                }
            
                self.open = openString.doubleValue
                self.close = closeString.doubleValue
                self.high = highString.doubleValue
                self.low = lowString.doubleValue
            
            default:
                println("HistoricTimeQuote(\(symbol) initialization failure")
        }
        
        if let adjustedCloseString = list[csvHeaderAdjustedClose]{
            self.adjustedClose = adjustedCloseString.doubleValue
        }
        if let parsedVolume:Int = list[csvHeaderVolume]?.toInt(){
            self.volume = parsedVolume
        }
    }
    
    //internals
    internal var layerº:CALayer?
    internal var layerScaleFactorº:CGFloat?
}

public func ==(lhs: HistoricTimedQuote, rhs: HistoricTimedQuote) -> Bool {
    return (lhs.high == rhs.high)
}

extension HistoricTimedQuote: Printable, DebugPrintable
{
    public var description: String {
        return "\(symbol)(\(symbolName)): date = \(time), high = \(high), low = \(low), open = \(open), close = \(close), volume = \(volume)"
    }
    
    public var debugDescription: String { return self.description }
}

//findMin findMax extension
extension HistoricTimedQuote {
    
    public static var max: HistoricTimedQuote {
        var maxQuote = HistoricTimedQuote()
        maxQuote.high = Double(UInt64.max)
        maxQuote.low = Double(UInt64.max)
        return maxQuote
    }
    
    public static func findMax(#quotes: [HistoricTimedQuote]) -> HistoricTimedQuote
    {
        return quotes.reduce(HistoricTimedQuote(), combine: { ($0.high > $1.high) ? $0 : $1 })
    }
    
    public static func findMin(#quotes: [HistoricTimedQuote]) -> HistoricTimedQuote
    {
        return quotes.reduce(HistoricTimedQuote.max, combine: { ($0.low < $1.low) ? $0 : $1 })
    }
}

// MARK: - Quote

public struct Quote: Printable, DebugPrintable
{
    public var symbol: String = String()
    public var symbolName: String = String()
    
    public var ask: Double = 0.0
    public var bid: Double = 0.0
    
    public var description: String {
        return "\(symbol)(\(symbolName)): bid = \(bid), ask = \(ask)"
    }
    
    public var debugDescription: String { return self.description }
}

public class LiveQuotesCentre
{
    public class func quotesFetchedFromYahoo(forSymbols symbols:[String]) -> [Quote]
    {
        let yahooDataSourceURLString = "http://finance.yahoo.com/d/quotes.csv"
        let yahooFetchRequestParameters = "nab"
        
        var yahooFetchURLString = yahooDataSourceURLString + "?s="
        for (symbolIndex:Int, symbol:String) in enumerate(symbols) {
            
            if symbolIndex != 0 {
                yahooFetchURLString += "+"
            }
                
            yahooFetchURLString += symbol
        }
        
        yahooFetchURLString += ("&f=" + yahooFetchRequestParameters)
        let fetchURL: NSURL = NSURL(string: yahooFetchURLString)!
        let dataReader: CSVParser = CSVParser(contentsOfURL: fetchURL)
        
        var quotes: [Quote] = []
        for (rowIndex:Int, rowElement:[String]) in enumerate(dataReader.rows) {
            quotes.append( Quote(symbol: symbols[rowIndex], symbolName: rowElement[0], ask: (rowElement[1] as NSString).doubleValue, bid: (rowElement[2] as NSString).doubleValue) )
        }
        
        return quotes
    }
    
    public class func historicalQuotesFetchedFromYahoo(forSymbol symbol:String, fromDate startDate: NSDate, toDate endDate:NSDate) -> [HistoricTimedQuote]
    {
        //literal constants
        let yahooHistoricDataSourceURLString = "http://ichart.finance.yahoo.com/table.csv"
        let yahooFetchRequestAtributes = "ignore=.csv"
        
        
        let calendarStartDateComponents: NSDateComponents = NSCalendar.currentCalendar().components(.CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: startDate)
        let calendarEndDateComponents: NSDateComponents = NSCalendar.currentCalendar().components(.CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: endDate)
        
        var yahooFetchURLString = yahooHistoricDataSourceURLString + "?s=" + symbol
        yahooFetchURLString += "&a=\(calendarStartDateComponents.day)&b=\(calendarStartDateComponents.month)&c=\(calendarStartDateComponents.year)"
        yahooFetchURLString += "&d=\(calendarEndDateComponents.day)&e=\(calendarEndDateComponents.month)&f=\(calendarEndDateComponents.year)"
        yahooFetchURLString += "&" + yahooFetchRequestAtributes
        
        let fetchURL: NSURL = NSURL(string: yahooFetchURLString)!
        let dataReader: CSVParser = CSVParser(contentsOfURL: fetchURL, options: .HeaderPresent)
        
        //we fetch a simple quoute just for fetching a full name
        let quote: Quote = LiveQuotesCentre.quotesFetchedFromYahoo(forSymbols: [symbol]).first!
        
        
        let historicQuotes: [HistoricTimedQuote] = dataReader.rows.map { row in
            return HistoricTimedQuote(symbol: symbol, symbolName: quote.symbolName, timeInterval: oneDayTimeInterval, propertyList: dataReader.rowPropertyList(forRow: row)!)
        }
        
        return historicQuotes
    }
}












