//
//  ChartViewController.swift
//  Tradeboard
//
//  Created by Taras Vozniuk on 9/7/14.
//  Copyright (c) 2014 ambientlight. All rights reserved.
//

import UIKit

public class ChartViewController: UIViewController {
    
    var historicQuotes: [HistoricTimedQuote] = []
    
    convenience public init(historicQuotes: [HistoricTimedQuote])
    {
        self.init()
        self.historicQuotes = historicQuotes
    }

    override public func viewDidLoad()
    {
        var startDateComponents: NSDateComponents = NSDateComponents()
        startDateComponents.day = 20; startDateComponents.month = 8; startDateComponents.year = 2012
        let startDate:NSDate = NSCalendar.currentCalendar().dateFromComponents(startDateComponents)!
        
        var historicalQuotes: [HistoricTimedQuote] = LiveQuotesCentre.historicalQuotesFetchedFromYahoo(forSymbol: "TSLA", fromDate: startDate, toDate: NSDate())
        self.historicQuotes = historicalQuotes.reverse()
        
        
        let chartView = Slicer(frame: self.view.bounds, historicQuotes: self.historicQuotes)
        self.view.addSubview(chartView)
    }
}
