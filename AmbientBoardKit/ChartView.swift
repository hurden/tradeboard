//
//  ChartView.swift
//  Tradeboard
//
//  Created by Taras Vozniuk on 9/4/14.
//  Copyright (c) 2014 ambientlight. All rights reserved.
//

import UIKit

private let defaultQuoteElementWidth:CGFloat = 11.0
private let defaultQuoteElementOffset:CGFloat = 3.0
private let defaultChartViewMargins = ChartViewMargins(top: 50.0, bottom: 50.0, right: 10.0)


public struct ChartViewMargins {
    var top: CGFloat = 0.0
    var bottom: CGFloat = 0.0
    
    var right: CGFloat = 0.0
}

public enum ChartViewQuoteElementStyle {
    case Candles
    case Bars
    case LineSegments
}

public struct ChartViewPriceInfo {
    var maximumPrice: Double = 0.0
    var minimumPrice: Double = 0.0
}

public class ChartView: UIScrollView, UIScrollViewDelegate {

    //MARK: - _TimelineView
    
    private class _TimelineView: UIView {
        
        unowned private let _ownerView: ChartView
        
        private var _quoteElementLayersMapping:Dictionary<CALayer, HistoricTimedQuote> = [:]
        private var _visibleQuoteElementsMaxCount: Int {
            return Int(_ownerView.bounds.width / (_ownerView.quoteElementWidth + _ownerView.quoteElementOffset))
        }
        
        private var _visibleHistoricQuotes: [HistoricTimedQuote] {
            var firstVisibleIndex = Int(ceil(_ownerView.contentOffset.x / (_ownerView.quoteElementWidth + _ownerView.quoteElementOffset)))
                
                if firstVisibleIndex >= _ownerView.historicQuotes.count {
                    return []
                } else if firstVisibleIndex < 0 {
                    return []
                } else if firstVisibleIndex < (_ownerView.historicQuotes.count - _visibleQuoteElementsMaxCount) {
                    return Array(_ownerView.historicQuotes[firstVisibleIndex ... (firstVisibleIndex + _visibleQuoteElementsMaxCount)])
                } else {
                    return Array(_ownerView.historicQuotes[firstVisibleIndex ..< _ownerView.historicQuotes.count])
                }
        }
        
        private var _minimumSourceCoordinate: CGFloat = 0.0
        private var _maximumSourceCoordinate: CGFloat = 0.0
        
        var minimumOwnCoordinate: CGFloat { return self.frame.height /* - self.margins.bottom*/ }
        var maximumOwnCoordinate: CGFloat { return 0.0 /*self.margins.top*/ }
        
        private var _visibleMaxQuote: HistoricTimedQuote = HistoricTimedQuote()
        private var _visibleMinQuote: HistoricTimedQuote = HistoricTimedQuote()
        
        private var _visibleQuoteMinimumCoordinate: CGFloat = 0.0
        private var _visibleQuoteMaximumCoordinate: CGFloat = 0.0
        
        //MARK: - implementation
        
        private var _resizeFraction: CGFloat = 1.0
        
        init(frame: CGRect, ownerView: ChartView) {
            _ownerView = ownerView
            _minimumSourceCoordinate = CGFloat(HistoricTimedQuote.findMin(quotes: _ownerView.historicQuotes).low)
      	      _maximumSourceCoordinate = CGFloat(HistoricTimedQuote.findMax(quotes: _ownerView.historicQuotes).high)
            
            super.init(frame: frame)
            self.backgroundColor = UIColor.clearColor()
            
            
            //initial scaling
            _visibleMinQuote = HistoricTimedQuote.findMin(quotes: _visibleHistoricQuotes)
            _visibleMaxQuote = HistoricTimedQuote.findMax(quotes: _visibleHistoricQuotes)
            _scale(visibleMinQuote: _visibleMinQuote, visibleMaxQuote: _visibleMaxQuote)
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("NSCoding is not supported")
        }
        
        override func drawRect(rect: CGRect)
        {
            let ctx:CGContext = UIGraphicsGetCurrentContext()
            
            for (let historicQuoteIndex: Int, var historicQuote: HistoricTimedQuote) in enumerate(_ownerView.historicQuotes) {
                _drawQuoteElement(forHistoricQuote: historicQuote, quoteIndex: historicQuoteIndex, inContext: ctx)
            }
        }
        
        func ownerViewDidScroll()
        {
            if _ownerView.contentOffset.x < 0.0 { return }
            
            _visibleMaxQuote = HistoricTimedQuote.findMax(quotes: _visibleHistoricQuotes)
            _visibleMinQuote = HistoricTimedQuote.findMin(quotes: _visibleHistoricQuotes)
            
            
            var visibleMappedMax = _mapQuotesCoordinateToOwnCoordinate(CGFloat(_visibleMaxQuote.high))
            var visibleMappedMin = _mapQuotesCoordinateToOwnCoordinate(CGFloat(_visibleMinQuote.low))
            if visibleMappedMin != _visibleQuoteMinimumCoordinate || visibleMappedMax != _visibleQuoteMaximumCoordinate {
                
                _descale()
                _scale(visibleMinQuote: _visibleMinQuote, visibleMaxQuote: _visibleMaxQuote)
            }
        }
        
        func ownerViewDidReceivePinchGesture(gestureRecognizer: UIPinchGestureRecognizer)
        {
            println("\(gestureRecognizer.scale)")
            
            _ownerView.margins.top /= 2*gestureRecognizer.scale
            _ownerView.margins.bottom /= 2*gestureRecognizer.scale
            
            if _ownerView.margins.top > 200 {
                _ownerView.margins.top = 200
            } else if _ownerView.margins.top < 5 {
                _ownerView.margins.top = 5
            }
            
            if _ownerView.margins.bottom > 200 {
                _ownerView.margins.bottom = 200
            } else if _ownerView.margins.bottom < 5 {
                _ownerView.margins.bottom = 5
            }
            

            _descale()
            _scale(visibleMinQuote: _visibleMinQuote, visibleMaxQuote: _visibleMaxQuote)
            
        }
        
        //MARK: internals
        
        private func _descale()
        {
            self.frame.origin.y = 0
            self.frame.size.height /= self._resizeFraction
        }
        
        private func _scale(#visibleMinQuote: HistoricTimedQuote, visibleMaxQuote: HistoricTimedQuote)
        {
            let visibleMappedMax = _mapQuotesCoordinateToOwnCoordinate(CGFloat(visibleMaxQuote.high))
            let visibleMappedMin = _mapQuotesCoordinateToOwnCoordinate(CGFloat(visibleMinQuote.low))
            
            let newResizeFraction = (UIScreen.mainScreen().bounds.size.height - _ownerView.margins.bottom - _ownerView.margins.top) / (visibleMappedMin - visibleMappedMax)
            
            self.frame.size.height *= newResizeFraction
            self.frame.origin.y = -(visibleMappedMax * newResizeFraction) + _ownerView.margins.top
            
            
            _visibleQuoteMaximumCoordinate = _mapQuotesCoordinateToOwnCoordinate(CGFloat(visibleMaxQuote.high))
            _visibleQuoteMinimumCoordinate = _mapQuotesCoordinateToOwnCoordinate(CGFloat(visibleMinQuote.low))
            _resizeFraction = newResizeFraction
        }
        
        private func _mapQuotesCoordinateToOwnCoordinate(originalCoordinate: CGFloat) -> CGFloat
        {
            let sourcePercentile = (originalCoordinate - _minimumSourceCoordinate) / (_maximumSourceCoordinate - _minimumSourceCoordinate)
            let sourcePercentileComplement = sourcePercentile
            //let sourcePercentileComplement = 1 - sourcePercentile
            
            //println("\(self.maximumViewsCoordinate), \(self.minimumViewsCoordinate)")
            
            let destinationInterval = (self.maximumOwnCoordinate - self.minimumOwnCoordinate) * sourcePercentileComplement
            return destinationInterval + self.minimumOwnCoordinate
        }
        
        private func _drawQuoteElement(forHistoricQuote historicQuote:HistoricTimedQuote, quoteIndex historicQuoteIndex:Int, inContext ctx:CGContext)
        {
            let quoteTimelineXPosition = CGFloat(historicQuoteIndex) * (_ownerView.quoteElementWidth + _ownerView.quoteElementOffset)
            let quoteTimelineYPosition = _mapQuotesCoordinateToOwnCoordinate(CGFloat(historicQuote.high))
            let quoteTimelineWidth = defaultQuoteElementWidth
            let quoteTimelineHight = _mapQuotesCoordinateToOwnCoordinate(CGFloat(historicQuote.low)) - _mapQuotesCoordinateToOwnCoordinate(CGFloat(historicQuote.high))
            
            let color:CGColor = (historicQuote.open <  historicQuote.close) ? UIColor.greenColor().CGColor : UIColor.redColor().CGColor
            
            CGContextSetFillColorWithColor(ctx, color)
            CGContextSetStrokeColorWithColor(ctx, color)
            
            CGContextBeginPath(ctx)
            CGContextMoveToPoint(ctx, quoteTimelineXPosition + (quoteTimelineWidth * 0.5), quoteTimelineYPosition)
            CGContextAddLineToPoint(ctx, quoteTimelineXPosition + (quoteTimelineWidth * 0.5), quoteTimelineYPosition + quoteTimelineHight)
            CGContextStrokePath(ctx)
            
            let candleBodyY = _mapQuotesCoordinateToOwnCoordinate(CGFloat(historicQuote.close)) - _mapQuotesCoordinateToOwnCoordinate(CGFloat(historicQuote.high))
            let candleBodyHeight = _mapQuotesCoordinateToOwnCoordinate(CGFloat(historicQuote.open)) - _mapQuotesCoordinateToOwnCoordinate(CGFloat(historicQuote.close))
            
            if abs(candleBodyHeight) <= 1.0 {
                CGContextBeginPath(ctx)
                CGContextMoveToPoint(ctx, quoteTimelineXPosition, quoteTimelineYPosition + candleBodyY)
                CGContextAddLineToPoint(ctx, quoteTimelineXPosition + quoteTimelineWidth, quoteTimelineYPosition + candleBodyY)
                CGContextStrokePath(ctx)
            } else {
                CGContextFillRect(ctx, CGRect(x: quoteTimelineXPosition, y: quoteTimelineYPosition + candleBodyY, width: quoteTimelineWidth, height: candleBodyHeight))
            }
        }
    }
    
    //MARK: - ChartView
    
    private let _timelineView: _TimelineView!
    private var _timelineViewFrame: CGRect = CGRect()
    
    private(set) var historicQuotes: [HistoricTimedQuote] = []
    
    
    var margins: ChartViewMargins = defaultChartViewMargins
    var quoteElementsStyle: ChartViewQuoteElementStyle = .Candles
    var quoteElementWidth: CGFloat = defaultQuoteElementWidth
    var quoteElementOffset:CGFloat = defaultQuoteElementOffset
    
    private var _pinchGestureRecognizerª: UIPinchGestureRecognizer!

    // MARK: - implementation
    
    public init(frame: CGRect, historicQuotes quotes: [HistoricTimedQuote]) {
        super.init(frame: frame)
        
        historicQuotes = quotes
        _initDrawingProperties()
        _timelineView = _TimelineView(frame: _timelineViewFrame, ownerView: self)
        self.addSubview(_timelineView)
        
        self.delegate = self
        self.backgroundColor = UIColor.clearColor()
        
        _pinchGestureRecognizerª = UIPinchGestureRecognizer(target: self, action: "userDidPinch:")
        self.addGestureRecognizer(_pinchGestureRecognizerª)
    }

    required public init(coder aDecoder: NSCoder) {
        fatalError("NSCoding is not supported")
    }
    
    
    // MARK: internals
    
    private func _initDrawingProperties()
    {
        //determine total timeline rectangle
        let timelineRectWidth = (self.quoteElementWidth + self.quoteElementOffset) * CGFloat(self.historicQuotes.count)
        let timelineRectHight = self.bounds.size.height
        
        let visibleQuoteElementsMaxCount = Int(self.bounds.width / (self.quoteElementWidth + self.quoteElementOffset))
        let timelineOffsetX = (timelineRectWidth - (CGFloat(visibleQuoteElementsMaxCount) * (self.quoteElementWidth + self.quoteElementOffset)))
        let timelineOffsetY = CGFloat(0.0)
        _timelineViewFrame = CGRect(origin: CGPointZero, size: CGSize(width: timelineRectWidth, height: timelineRectHight))
        println("timelineFrame: \(_timelineViewFrame)")
        
        //scollView content size and offset
        self.contentSize = _timelineViewFrame.size
        self.contentOffset = CGPoint(x: timelineOffsetX, y: timelineOffsetY)
    }
    
    
    // MARK: - UIScrollViewDelegate
    public func scrollViewDidScroll(scrollView: UIScrollView)
    {
        _timelineView.ownerViewDidScroll()
    }
    
    public func userDidPinch(gestureRecognizer: UIPinchGestureRecognizer)
    {
        _timelineView.ownerViewDidReceivePinchGesture(gestureRecognizer)
    }
}