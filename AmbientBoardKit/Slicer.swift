//
//  Slicer.swift
//  Tradeboard
//
//  Created by Taras on 10/30/14.
//  Copyright (c) 2014 ambientlight. All rights reserved.
//

import Foundation
import UIKit

private let defaultQuoteElementWidth:CGFloat = 11.0
private let defaultQuoteElementOffset:CGFloat = 3.0
private let defaultChartViewMargins = ChartViewMargins(top: 50.0, bottom: 50.0, right: 10.0)


public class Slicer:UIView {
    
    private(set) var historicQuotes: [HistoricTimedQuote] = []
    var margins: ChartViewMargins = defaultChartViewMargins
    var quoteElementsStyle: ChartViewQuoteElementStyle = .Candles
    var quoteElementWidth: CGFloat = defaultQuoteElementWidth
    var quoteElementOffset:CGFloat = defaultQuoteElementOffset
    
    //debug-on-playground
    var totalQuoteElementsWidth: CGFloat = 0.0
    var lastSliceX: CGFloat = 0.0
    var lastSliceQuoteStartIndex: Int = 0
    var visibleQuotes: [HistoricTimedQuote] = []
    /*private*/ var _visibleMinQuote: HistoricTimedQuote = HistoricTimedQuote()
    /*private*/ var _visibleMaxQuote: HistoricTimedQuote = HistoricTimedQuote()
    
    
    public init(frame: CGRect, historicQuotes quotes: [HistoricTimedQuote])
    {
        super.init(frame: frame)
        historicQuotes = quotes
        self.backgroundColor = UIColor.clearColor()
        
        _initDrawingProperties()
    }
    
    private func _initDrawingProperties()
    {
        let visibleQuotesCount = Int(floor(self.bounds.size.width / (self.quoteElementWidth + self.quoteElementOffset)))
        for (var sliceQuoteStartIndex:Int = 0 ; sliceQuoteStartIndex < self.historicQuotes.count; sliceQuoteStartIndex += visibleQuotesCount)
        {
            
        }
        
        
        self.totalQuoteElementsWidth = (self.quoteElementWidth + self.quoteElementOffset)*CGFloat(self.historicQuotes.count)
        self.lastSliceX = totalQuoteElementsWidth - self.bounds.size.width;
        self.lastSliceQuoteStartIndex = Int(ceil(self.lastSliceX / (self.quoteElementWidth + self.quoteElementOffset)))
        
        self.visibleQuotes = Array(self.historicQuotes[self.lastSliceQuoteStartIndex ..< self.historicQuotes.count])
        _visibleMinQuote = HistoricTimedQuote.findMin(quotes: self.visibleQuotes)
        _visibleMaxQuote = HistoricTimedQuote.findMax(quotes: self.visibleQuotes)
        
    }
    
    public override func drawLayer(layerª: CALayer!, inContext ctx: CGContext!)
    {
        if layerª == self.layer {
            super.drawLayer(layerª, inContext: ctx)
        } else if let layer = layerª {
            
            
        }
    }
    
    override public func drawRect(rect: CGRect)
    {
        let context:CGContext = UIGraphicsGetCurrentContext()
        let initialRange:(CGFloat, CGFloat) = (CGFloat(_visibleMinQuote.low), CGFloat(_visibleMaxQuote.high))
        
        
        for (let quoteIndex: Int, var quote:HistoricTimedQuote) in enumerate(self.visibleQuotes){
            
            let quoteXPosition = CGFloat(quoteIndex) * (self.quoteElementWidth + self.quoteElementOffset)
            let quoteYPosition = _mapFromRange(initialRange, coordinate: CGFloat(quote.high))
            let quoteWidth = defaultQuoteElementWidth
            let quoteHeight = _mapFromRange(initialRange, coordinate: CGFloat(quote.low)) - quoteYPosition
            
            let color:CGColor = (quote.open <  quote.close) ? UIColor.greenColor().CGColor : UIColor.redColor().CGColor
            
            CGContextSetFillColorWithColor(context, color)
            CGContextSetStrokeColorWithColor(context, color)
            
            CGContextBeginPath(context)
            CGContextMoveToPoint(context, quoteXPosition + (quoteWidth * 0.5), quoteYPosition)
            CGContextAddLineToPoint(context, quoteXPosition + (quoteWidth * 0.5), quoteYPosition + quoteHeight)
            CGContextStrokePath(context)
            
            let candleBodyY = _mapFromRange(initialRange, coordinate: CGFloat(quote.close))-_mapFromRange(initialRange, coordinate:CGFloat(quote.high))
            let candleBodyHeight = _mapFromRange(initialRange, coordinate: CGFloat(quote.open))-_mapFromRange(initialRange, coordinate:CGFloat(quote.close
                ))
            
            if abs(candleBodyHeight) <= 1.0 {
                CGContextBeginPath(context)
                CGContextMoveToPoint(context, quoteXPosition, quoteYPosition + candleBodyY)
                CGContextAddLineToPoint(context, quoteXPosition + quoteWidth, quoteYPosition + candleBodyY)
                CGContextStrokePath(context)
            } else {
                CGContextFillRect(context, CGRectMake(quoteXPosition, quoteYPosition + candleBodyY, quoteWidth, candleBodyHeight))
            }
        }
        
    }
    
    required public init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    /*private*/ func _mapFromRange(originRange: (CGFloat, CGFloat), coordinate: CGFloat) -> CGFloat
    {
        let (originRangeMin:CGFloat, originRangeMax:CGFloat) = originRange
        let sourcePercentile = (coordinate - originRangeMin)/(originRangeMax - originRangeMin)
        
        let ownCoordinateMin:CGFloat = self.frame.height
        let ownCoordinateMax:CGFloat = 0.0
        
        
        let destinationInterval = (ownCoordinateMax - ownCoordinateMin) * sourcePercentile
        return (ownCoordinateMin + destinationInterval)
    }
}
