//
//  notes.swift
//  Tradeboard
//
//  Created by Taras Vozniuk on 8/30/14.
//  Copyright (c) 2014 ambientlight. All rights reserved.
//



/* The difference between unowned and weak 
 *
 * is that weak is declared as an Optional while unowned is not. 
 * By declaring it weak you get to handle the case that it might be nil inside the closure at some point. 
 * If you try to access an unowned variable that happens to be nil, it will crash the whole program. 
 * So only use unowned when you are positive that variable will always be around while the closure is around
 */

/* @autoclosure 
 *
 * creates an automatic closure around the expression. 
 * So when the caller writes an expression like 2 > 1, it's automatically wrapped into a closure to become {2 > 1} before it is passed to f. 
 * So if we apply this to the function f

func f(pred: @autoclosure () -> Bool) {
    if pred() {
        println("It's true")
    }
}

f(2 > 1)
// It's true
*/

/*
// key, value in dictionary
[ 0: 1, 2: 5, 8: 10 ].map({ (k, v) in k + v }) // -> [ 1, 7, 18 ]

// value in dictionary
[ 0: 1, 2: 5, 8: 10 ].map({ v in v * 2 }) // -> [ 1, 10, 20 ]

// key, value in dictionary to dictionary
[ 0: 1, 2: 5, 8: 10 ].map({ (k, v) in (v, k) }) // -> [ 1: 0, 5: 2, 10: 8 ]
*/

